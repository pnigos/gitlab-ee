export default () => ({
  configData: null,
  configLoading: true,
  activeTab: null,
  activeChart: null,
  chartLoading: false,
  chartData: null,
});
